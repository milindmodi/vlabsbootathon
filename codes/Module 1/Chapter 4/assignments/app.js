var t1 = document.getElementById("t1");
var t2 = document.getElementById("t2");
var t3 = document.getElementById("t3");
function checkEmpty() {
    if (t1.value === "" || t2.value === "") {
        alert("Fill all the text box");
        return -1;
    }
    return 1;
}
function add() {
    if (checkEmpty() == -1) {
        return;
    }
    var c = parseFloat(t1.value) + parseFloat(t2.value);
    t3.value = c.toString();
}
function sub() {
    if (checkEmpty() == -1) {
        return;
    }
    var c = parseFloat(t1.value) - parseFloat(t2.value);
    t3.value = c.toString();
}
function mul() {
    if (checkEmpty() == -1) {
        return;
    }
    var c = parseFloat(t1.value) * parseFloat(t2.value);
    t3.value = c.toString();
}
function div() {
    if (checkEmpty() == -1) {
        return;
    }
    if (parseFloat(t2.value) == 0) {
        alert("divide by \\0 exception");
        return;
    }
    var c = parseFloat(t1.value) / parseFloat(t2.value);
    t3.value = c.toString();
}
