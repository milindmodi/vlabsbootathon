//-------------------------------------------------Assignment 2 ------------------------------------------//
function area_triangle() {
    var d11 = document.getElementById('d11');
    var d12 = document.getElementById('d12');
    var d21 = document.getElementById('d21');
    var d22 = document.getElementById('d22');
    var d31 = document.getElementById('d31');
    var d32 = document.getElementById('d32');
    var a1 = document.getElementById('a1');
    if (d11.value == "" || d12.value == "" || d21.value == "" || d22.value == "" || d31.value == "" || d32.value == "") {
        alert("fill all the values");
        return;
    }
    var x1 = parseFloat(d11.value);
    var y1 = parseFloat(d12.value);
    var x2 = parseFloat(d21.value);
    var y2 = parseFloat(d22.value);
    var x3 = parseFloat(d31.value);
    var y3 = parseFloat(d32.value);
    if (isNaN(x1) || isNaN(y1) || isNaN(x2) || isNaN(y2) || isNaN(x3) || isNaN(y3)) {
        alert("Enter number only");
        return;
    }
    var a = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
    var b = Math.sqrt(Math.pow((x1 - x3), 2) + Math.pow((y1 - y3), 2));
    var c = Math.sqrt(Math.pow((x3 - x2), 2) + Math.pow((y3 - y2), 2));
    var s = (a + b + c) / 2;
    var area = Math.sqrt(s * (s - a) * (s - b) * (s - c));
    a1.innerHTML = "Answer is: " + area.toString();
}
