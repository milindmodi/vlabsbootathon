
//-------------------------------------------------Assignment 2 ------------------------------------------//
function area_triangle(){
    let d11:HTMLInputElement = <HTMLInputElement>document.getElementById('d11');
    let d12:HTMLInputElement = <HTMLInputElement>document.getElementById('d12');
    let d21:HTMLInputElement = <HTMLInputElement>document.getElementById('d21');
    let d22:HTMLInputElement = <HTMLInputElement>document.getElementById('d22');
    let d31:HTMLInputElement = <HTMLInputElement>document.getElementById('d31');
    let d32:HTMLInputElement = <HTMLInputElement>document.getElementById('d32');
    let a1:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById('a1');

    if(d11.value == "" ||d12.value == "" ||d21.value == "" ||d22.value == "" ||d31.value == "" ||d32.value == ""){
        alert("fill all the values");
        return;
    }
    var x1 : number = parseFloat(d11.value);
    var y1 : number = parseFloat(d12.value);
    var x2 : number = parseFloat(d21.value);
    var y2 : number = parseFloat(d22.value);
    var x3 : number = parseFloat(d31.value);
    var y3 : number = parseFloat(d32.value);

    if(isNaN(x1) ||isNaN(y1) ||isNaN(x2) ||isNaN(y2) ||isNaN(x3) ||isNaN(y3)  ){
        alert("Enter number only");
        return;
    }
    
    var a = Math.sqrt(Math.pow((x2-x1), 2) + Math.pow((y2-y1), 2));
   
    var b = Math.sqrt(Math.pow((x1-x3), 2) + Math.pow((y1-y3), 2));
    
    var c = Math.sqrt(Math.pow((x3-x2), 2) + Math.pow((y3-y2), 2));
 
    var s = (a + b + c) / 2;
    var area = Math.sqrt(s * (s - a) * (s - b) * (s - c));
    a1.innerHTML = "Answer is: " + area.toString();

}