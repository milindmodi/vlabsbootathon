var num1;
var num2;
function add() {
    var t1 = document.getElementById("t1");
    var t2 = document.getElementById("t2");
    if (t1.value == "" || t2.value == "") {
        t1.focus();
        alert("please fill  both the box");
        return;
    }
    num1 = parseFloat(t1.value);
    num2 = parseFloat(t2.value);
    if (isNaN(num1) || isNaN(num2)) {
        alert("please fill  numbers only");
        return;
    }
    t1.value = (num1 + num2).toString();
    t2.value = "";
}
function sub() {
    var t1 = document.getElementById("t1");
    var t2 = document.getElementById("t2");
    if (t1.value == "" || t2.value == "") {
        t1.focus();
        alert("please fill  both the box");
        return;
    }
    num1 = parseFloat(t1.value);
    num2 = parseFloat(t2.value);
    if (isNaN(num1) || isNaN(num2)) {
        alert("please fill  numbers only");
        return;
    }
    t1.value = (num1 - num2).toString();
    t2.value = "";
}
function div() {
    var t1 = document.getElementById("t1");
    var t2 = document.getElementById("t2");
    if (t1.value == "" || t2.value == "") {
        t1.focus();
        alert("please fill  both the box");
        return;
    }
    num1 = parseFloat(t1.value);
    num2 = parseFloat(t2.value);
    if (isNaN(num1) || isNaN(num2)) {
        alert("please fill  numbers only");
        return;
    }
    if (num2 == 0) {
        alert("Can't divide by zero ");
        t2.focus();
        return;
    }
    t1.value = (num1 / num2).toString();
    t2.value = "";
}
function mul() {
    var t1 = document.getElementById("t1");
    var t2 = document.getElementById("t2");
    if (t1.value == "" || t2.value == "") {
        t1.focus();
        alert("please fill  both the box");
        return;
    }
    num1 = parseFloat(t1.value);
    num2 = parseFloat(t2.value);
    if (isNaN(num1) || isNaN(num2)) {
        alert("please fill  numbers only");
        return;
    }
    t1.value = (num1 * num2).toString();
    t2.value = "";
}
function sin1() {
    var t1 = document.getElementById("t1");
    var t2 = document.getElementById("t2");
    var s1 = document.getElementById("s1");
    if (t1.value == "") {
        t1.focus();
        alert("please fill the first box properly");
        return;
    }
    num1 = parseFloat(t1.value);
    if (isNaN(num1)) {
        alert("please fill  numbers only");
        return;
    }
    if (s1.value == "deg") {
        console.log("degree");
        var x1 = Math.PI / 180 * parseFloat(t1.value);
        console.log(x1);
        var ans1 = Math.sin(x1);
        t2.value = ans1.toString();
    }
    else if (s1.value == "rad") {
        console.log("Radian");
        var ans1 = Math.sin(parseFloat(t1.value));
        t2.value = ans1.toString();
    }
}
function cos1() {
    var t1 = document.getElementById("t1");
    var t2 = document.getElementById("t2");
    var s1 = document.getElementById("s1");
    if (t1.value == "") {
        t1.focus();
        alert("please fill the first box properly");
        return;
    }
    num1 = parseFloat(t1.value);
    if (isNaN(num1)) {
        alert("please fill  numbers only");
        return;
    }
    if (s1.value == "deg") {
        console.log("degree");
        var x1 = Math.PI / 180 * parseFloat(t1.value);
        console.log(x1);
        var ans1 = Math.cos(x1);
        t2.value = ans1.toString();
    }
    else if (s1.value == "rad") {
        console.log("Radian");
        var ans1 = Math.cos(parseFloat(t1.value));
        t2.value = ans1.toString();
    }
}
function tan1() {
    var t1 = document.getElementById("t1");
    var t2 = document.getElementById("t2");
    var s1 = document.getElementById("s1");
    if (t1.value == "") {
        t1.focus();
        alert("please fill the first box properly");
        return;
    }
    num1 = parseFloat(t1.value);
    if (isNaN(num1)) {
        alert("please fill  numbers only");
        return;
    }
    if (s1.value == "deg") {
        console.log("degree");
        var x1 = Math.PI / 180 * parseFloat(t1.value);
        console.log(x1);
        var ans1 = Math.tan(x1);
        t2.value = ans1.toString();
    }
    else if (s1.value == "rad") {
        console.log("Radian");
        var ans1 = Math.tan(parseFloat(t1.value));
        t2.value = ans1.toString();
    }
}
function sqrt1() {
    var t1 = document.getElementById("t1");
    var t2 = document.getElementById("t2");
    if (t1.value == "") {
        t1.focus();
        alert("please fill the first box properly");
        return;
    }
    num1 = parseFloat(t1.value);
    if (isNaN(num1)) {
        alert("please fill  numbers only");
        return;
    }
    var ans1 = Math.sqrt(num1);
    t2.value = ans1.toString();
}
function power1() {
    var t1 = document.getElementById("t1");
    var t2 = document.getElementById("t2");
    if (t1.value == "") {
        t1.focus();
        alert("please fill the first box properly");
        return;
    }
    num1 = parseFloat(t1.value);
    if (isNaN(num1)) {
        alert("please fill  numbers only");
        return;
    }
    var ans1 = Math.pow(num1, 2);
    t2.value = ans1.toString();
}
