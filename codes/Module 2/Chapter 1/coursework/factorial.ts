function fact(){
    var num:HTMLInputElement = <HTMLInputElement>document.getElementById("num");
    var ans:HTMLInputElement = <HTMLInputElement>document.getElementById("ans");
    var count:number = parseInt(num.value);
    var fac:number = 1;;
    while(count >= 1){
            fac = fac * count;
            count--;
    } 

    ans.value = fac.toString();
}