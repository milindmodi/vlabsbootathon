function for_loop() {
    var count;
    for (count = 1; count <= 100; count++) {
        console.log(count);
    }
}
function while_loop() {
    var count = 200;
    while (count > 100) {
        console.log(count);
        count--;
    }
}
function do_while_loop() {
    var count = 200;
    do {
        console.log(count);
        count--;
    } while (count > 100);
}
