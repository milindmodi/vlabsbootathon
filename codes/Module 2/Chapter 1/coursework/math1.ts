function sin1(){
    var t1: HTMLInputElement=<HTMLInputElement>document.getElementById("t11");
    let t2: HTMLInputElement=<HTMLInputElement>document.getElementById("t12");
    var a:number = Math.PI / 180 * parseFloat(t1.value);
    var b:number = Math.sin(a);
    t2.value = b.toString();
}
function cos1(){
    var t1: HTMLInputElement=<HTMLInputElement>document.getElementById("t21");
    let t2: HTMLInputElement=<HTMLInputElement>document.getElementById("t22");
    var a:number = Math.PI / 0 * parseFloat(t1.value);
    var b:number = Math.cos(a);
    t2.value = b.toString();
}
function area_circle(){
    var t1: HTMLInputElement=<HTMLInputElement>document.getElementById("t31");
    let t2: HTMLInputElement=<HTMLInputElement>document.getElementById("t32");
    var a:number = Math.PI * Math.pow(parseFloat(t1.value),2); 
    t2.value = a.toString();
}