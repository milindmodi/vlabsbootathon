function calculate() {
    let a : HTMLInputElement = <HTMLInputElement>document.getElementById('d1');
    let b : HTMLInputElement = <HTMLInputElement>document.getElementById('d2');
    let c : HTMLInputElement = <HTMLInputElement>document.getElementById('d3');

    let l1 : HTMLInputElement = <HTMLInputElement>document.getElementById('l1');
    let l2 : HTMLInputElement = <HTMLInputElement>document.getElementById('l2');

    let x : number = +a.value;
    let y : number = +b.value;
    let z : number = +c.value;

    l2.innerHTML = "";

    if((x == y) && (y == z)) {
        l1.innerHTML = "It is an equilateral triangle.";
    }
    else if((x == y) || (y == z) || (x == z)) {
        l1.innerHTML = "It is an isosceles triangle.";
        if((Math.pow(x, 2) == Math.pow(y, 2) + Math.pow(z, 2)) || (Math.pow(y, 2) == Math.pow(x, 2) + Math.pow(z, 2)) || (Math.pow(z, 2) == Math.pow(y, 2) + Math.pow(x, 2)))
            l2.innerHTML = "It is also a right angle triangle.";
    }
    else if((x != y) && (y != z) && (x != z)) {
        l1.innerHTML = "It is an scalene triangle.";
        if((Math.pow(x, 2) == Math.pow(y, 2) + Math.pow(z, 2)) || (Math.pow(y, 2) == Math.pow(x, 2) + Math.pow(z, 2)) || (Math.pow(z, 2) == Math.pow(y, 2) + Math.pow(x, 2)))
            l2.innerHTML = "It is also a right angle triangle.";
    }
}