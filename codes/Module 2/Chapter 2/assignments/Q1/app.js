//-------------------------------Assignment 1 -------------------------------------------//
function find_script() {
    var t1 = document.getElementById("t1");
    var a1 = document.getElementById("a1");
    if (t1.value == "") {
        alert("insert value first ");
        return;
    }
    var index = t1.value.toUpperCase().indexOf("SCRIPT");
    if (index >= 0)
        a1.value = t1.value.substr(index, index + ("SCRIPT").length);
    else
        a1.value = "Script not found";
}
function pos_e() {
    var t1 = document.getElementById("t1");
    var a2 = document.getElementById("a2");
    if (t1.value == "") {
        alert("insert value first ");
        return;
    }
    var index = t1.value.toUpperCase().indexOf("E");
    if (index >= 0)
        a2.value = "'E' found at position " + index;
    else
        a2.value = "Character 'E' not found";
}
