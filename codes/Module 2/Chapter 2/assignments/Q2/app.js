//-------------------------------Assignment 2 -------------------------------------------//
function convert_upper() {
    var t1 = document.getElementById("t1");
    var a1 = document.getElementById("a1");
    if (t1.value == "") {
        alert("insert value first ");
        return;
    }
    a1.value = t1.value.toUpperCase();
}
function convert_lower() {
    var t1 = document.getElementById("t1");
    var a2 = document.getElementById("a2");
    if (t1.value == "") {
        alert("insert value first ");
        return;
    }
    a2.value = t1.value.toLowerCase();
}
function split_space() {
    var t1 = document.getElementById("t1");
    var a3 = document.getElementById("a3");
    if (t1.value == "") {
        alert("insert value first ");
        return;
    }
    a3.innerHTML = "";
    var values = t1.value.split(" ");
    var count = 0;
    for (count = 0; count < values.length; count++)
        a3.innerHTML += "<br /> <li>" + values[count] + "</li>";
}
