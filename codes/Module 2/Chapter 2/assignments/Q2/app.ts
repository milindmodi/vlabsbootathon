
//-------------------------------Assignment 2 -------------------------------------------//
function convert_upper(){
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1"); 
    let a1:HTMLInputElement = <HTMLInputElement>document.getElementById("a1");  
    if(t1.value == ""){
        alert("insert value first ");
        return;
    }
    a1.value = t1.value.toUpperCase()
}
function convert_lower(){
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1"); 
    let a2:HTMLInputElement = <HTMLInputElement>document.getElementById("a2");  
    if(t1.value == ""){
        alert("insert value first ");
        return;
    }
    a2.value = t1.value.toLowerCase()
}
function split_space(){
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1"); 
    let a3:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("a3");  
    if(t1.value == ""){
        alert("insert value first ");
        return;
    }
    a3.innerHTML ="";
    let values:Array<string> =  t1.value.split(" ");
    let count:number = 0;
    for(count = 0; count < values.length; count++)
        a3.innerHTML += "<br /> <li>" + values[count] +"</li>";
}