//-------------------------------Assignment 1 -------------------------------------------//
function even_odd() {
    var t1 = document.getElementById("t1");
    var a1 = document.getElementById("a1");
    var num1;
    if (t1.value == "") {
        alert("insert value first ");
        return;
    }
    num1 = parseFloat(t1.value);
    if (isNaN(num1)) {
        alert("please fill  numbers only");
        return;
    }
    if (num1 % 2 == 0) {
        a1.value = "Entered number is Even";
    }
    else {
        a1.value = "Entered number is Odd";
    }
}
