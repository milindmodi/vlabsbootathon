//-------------------------------Assignment 2 -------------------------------------------//
function calculate_triangle() {
    var d1 = document.getElementById('d1');
    var d2 = document.getElementById('d2');
    var d3 = document.getElementById('d3');
    if (d1.value == "" || d2.value == "" || d3.value == "") {
        alert("insert all the values first ");
        return;
    }
    var l1 = document.getElementById('l1');
    var l2 = document.getElementById('l2');
    var x = +d1.value;
    var y = +d2.value;
    var z = +d3.value;
    if (isNaN(x) || isNaN(y) || isNaN(z)) {
        alert("please enter  numbers only");
        return;
    }
    l2.innerHTML = "";
    if ((x == y) && (y == z)) {
        l1.innerHTML = "It is an equilateral triangle.";
    }
    else if ((x == y) || (y == z) || (x == z)) {
        l1.innerHTML = "It is an isosceles triangle.";
        if ((Math.pow(x, 2) == Math.pow(y, 2) + Math.pow(z, 2)) || (Math.pow(y, 2) == Math.pow(x, 2) + Math.pow(z, 2)) || (Math.pow(z, 2) == Math.pow(y, 2) + Math.pow(x, 2)))
            l2.innerHTML = "It is also a right angle triangle.";
    }
    else if ((x != y) && (y != z) && (x != z)) {
        l1.innerHTML = "It is an scalene triangle.";
        if ((Math.pow(x, 2) == Math.pow(y, 2) + Math.pow(z, 2)) || (Math.pow(y, 2) == Math.pow(x, 2) + Math.pow(z, 2)) || (Math.pow(z, 2) == Math.pow(y, 2) + Math.pow(x, 2)))
            l2.innerHTML = "It is also a right angle triangle.";
    }
}
