 
//-------------------------------Assignment 2 -------------------------------------------//
function calculate_triangle() {
    let d1 : HTMLInputElement = <HTMLInputElement>document.getElementById('d1');
    let d2 : HTMLInputElement = <HTMLInputElement>document.getElementById('d2');
    let d3 : HTMLInputElement = <HTMLInputElement>document.getElementById('d3');

    if(d1.value == "" || d2.value == "" || d3.value == ""){
        alert("insert all the values first ");
        return;
    }


    let l1 : HTMLInputElement = <HTMLInputElement>document.getElementById('l1');
    let l2 : HTMLInputElement = <HTMLInputElement>document.getElementById('l2');

    let x : number = +d1.value;
    let y : number = +d2.value;
    let z : number = +d3.value;
    if(isNaN(x) || isNaN(y) || isNaN(z)){
        alert("please enter  numbers only");
        return;
    }
    l2.innerHTML = "";

    if((x == y) && (y == z)) {
        l1.innerHTML = "It is an equilateral triangle.";
    }
    else if((x == y) || (y == z) || (x == z)) {
        l1.innerHTML = "It is an isosceles triangle.";
        if((Math.pow(x, 2) == Math.pow(y, 2) + Math.pow(z, 2)) || (Math.pow(y, 2) == Math.pow(x, 2) + Math.pow(z, 2)) || (Math.pow(z, 2) == Math.pow(y, 2) + Math.pow(x, 2)))
            l2.innerHTML = "It is also a right angle triangle.";
    }
    else if((x != y) && (y != z) && (x != z)) {
        l1.innerHTML = "It is an scalene triangle.";
        if((Math.pow(x, 2) == Math.pow(y, 2) + Math.pow(z, 2)) || (Math.pow(y, 2) == Math.pow(x, 2) + Math.pow(z, 2)) || (Math.pow(z, 2) == Math.pow(y, 2) + Math.pow(x, 2)))
            l2.innerHTML = "It is also a right angle triangle.";
    }
}

 