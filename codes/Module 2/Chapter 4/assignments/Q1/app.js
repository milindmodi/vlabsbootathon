//-------------------------------Assignment 1 -------------------------------------------//
function ask_number() {
    var t1 = document.getElementById("t1");
    var a1 = document.getElementById("a1");
    var num1;
    if (t1.value == "") {
        alert("insert value first ");
        return;
    }
    num1 = parseInt(t1.value);
    if (isNaN(num1)) {
        alert("please fill  numbers only");
        return;
    }
    var count = 0;
    var positive = 0;
    var negative = 0;
    var zeros = 0;
    for (count = 1; count <= num1; count++) {
        var temp = prompt("Enter number " + count + " : ");
        var num2 = parseFloat(temp);
        if (isNaN(num2)) {
            alert("please fill  numbers only");
            count--;
            continue;
        }
        if (num2 > 0) {
            positive += 1;
        }
        else if (num2 < 0) {
            negative += 1;
        }
        else if (num2 == 0) {
            zeros += 1;
        }
    }
    a1.innerHTML = "<br /> <li>Total Positive number : " + positive.toString() + "</li>";
    a1.innerHTML += " <li>Total negative number : " + negative.toString() + "</li>";
    a1.innerHTML += " <li>Total zero  : " + zeros.toString() + "</li>";
}
