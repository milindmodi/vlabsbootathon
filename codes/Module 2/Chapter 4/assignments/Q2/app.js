//-------------------------------Assignment 2 -------------------------------------------//
function checkArmstrong(num) {
    var temp = num;
    var sum = 0;
    while (temp > 0) {
        var digit = temp % 10;
        temp = parseInt((temp / 10).toString());
        sum = sum + (digit * digit * digit);
    }
    if (sum == num) {
        console.log("return 1 for " + num);
        return 1;
    }
    else {
        console.log("return 0 for " + num);
        return 0;
    }
}
function print_armstrong() {
    var tbl = document.getElementById("tbl");
    var count = 1;
    while (tbl.rows.length > 1) {
        tbl.deleteRow(1);
    }
    for (count = 100; count <= 999; count++) {
        if (checkArmstrong(count) == 1) {
            console.log("sum ");
            var row = tbl.insertRow();
            var cell = row.insertCell();
            var text = document.createElement("input");
            text.type = "text";
            text.style.textAlign = "center";
            text.value = count.toString();
            cell.appendChild(text);
        }
    }
}
